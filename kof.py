import os
import subprocess
import requests
from bs4 import BeautifulSoup

URL_PERSONAJES = 'http://es.kof.wikia.com/wiki/Personajes'
URL_PERSONAJE = 'http://es.kof.wikia.com/wiki/'

def obtener_imagen(url):
    nombre = url.split('/')[-1]
    html = requests.get(url)
    soup = BeautifulSoup(html.content, 'html.parser')

    try:
        tabla = soup.find_all('table', { 'class': 'darktable' })[0]
        imagen = tabla.find_all('img')[0].get('src')
        subprocess.call(['curl', imagen, '-o', os.path.join('imagenes', nombre + '.png')])
    except:
        print('NO PUDE ENCONTRAR LA IMAGEN DE ' + nombre)

def obtener_personajes():
    html_personajes = requests.get(URL_PERSONAJES)
    soup = BeautifulSoup(html_personajes.content, 'html.parser')

    personajes = soup.find_all('a', { 'class': 'image image-thumbnail link-external' })
    enlaces = [x.get('href') for x in personajes]

    # enlaces adicionales
    enlaces += ['http://es.kof.wikia.com/wiki/Zero_(Clon)', 'http://es.kof.wikia.com/wiki/Zero_(Original)']

    for enlace in enlaces:
        obtener_imagen(enlace)

def main():
    obtener_personajes()

if __name__ == '__main__':
    main()
